﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Vbe.Interop;

namespace AvitoAd
{
    class Ad
    {
        public readonly string article;
        public readonly string name;
        public readonly string price;
        public readonly string subCategory;
        public readonly string picture;

        public Ad(string article, string name, string price, string picture, string subCategory)
        {
            this.article = article;
            this.name = name;
            this.price = price;
            this.picture = picture;
            this.subCategory = subCategory;
        }

        public override string ToString() => string.Join(",", article, name, price, picture, subCategory);
    }
}
