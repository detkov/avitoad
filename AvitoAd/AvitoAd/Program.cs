﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace AvitoAd
{
    class Program
    {
        private static string description;
        private static int currentId;
        private static string phone;
        private static string category;
        private static List<Ad> ads = new List<Ad>();

        static void Main(string[] args)
        {
            GetAdParams();
            GetAds();
            CreateXML();
            updateId();
        }

        private static void updateId() => File.WriteAllText("currentId.txt", currentId.ToString());

        private static void CreateXML()
        {
            var xdoc = new XDocument();
            XElement Ads = new XElement("Ads");
            XAttribute formatVersion = new XAttribute("formatVersion", "3");
            XAttribute target = new XAttribute("target", "Avito.ru");
            Ads.Add(formatVersion);
            Ads.Add(target);
            foreach (var ad in ads)
            {
                var Ad = new XElement("Ad");
                var Id = new XElement("Id", currentId);
                var Category = new XElement("Category", category);
                var TypeId = new XElement("TypeId", ad.subCategory);
                var ContactPhone = new XElement("ContactPhone", phone);
                var Region = new XElement("Region", "Свердловская область");
                var City = new XElement("City", "Екатеринбург");
                var text = $"{ad.article}\n{ad.name}\n{description}";
                var Description = new XElement("Description", text);
                var Price = new XElement("Price", ad.price);
                var Images = new XElement("Images");
                var Image = new XElement("Image");
                Image.Add(new XAttribute("url", ad.picture));
                Images.Add(Image);
                currentId++;
                Ad.Add(Id, Category, TypeId, ContactPhone, Region, City, Description, Price, Images);
                Ads.Add(Ad);
            }
            xdoc.Add(Ads);
            xdoc.Save("avito.xml");
        }

        private static void GetAds()
        {
            var xlApp = new Excel.Application();
            var xlWorkbook = xlApp.Workbooks.Open($"{Directory.GetCurrentDirectory()}\\test.xlsx");
            var xlWorksheet = xlWorkbook.Sheets[1];
            var xlRange = xlWorksheet.UsedRange;
            var rowCount = xlRange.Rows.Count;
            var colCount = xlRange.Columns.Count;
            for (var i = 3; i <= rowCount; i++)
            {
                if (xlRange.Cells[i, 1].Value2 == null && xlRange.Cells[i, 2].Value2 == null && xlRange.Cells[i, 3].Value2 == null &&
                    xlRange.Cells[i, 6].Value2 == null && xlRange.Cells[i, 7].Value2 == null)
                {
                    break;
                }
                if (xlRange.Cells[i, 1].Value2 == null || xlRange.Cells[i, 2].Value2 == null || xlRange.Cells[i, 3].Value2 == null ||
                    xlRange.Cells[i, 6].Value2 == null || xlRange.Cells[i, 7].Value2 == null)
                {
                    Console.WriteLine($"В строке {i} в Excel файле ошибка - не все нужные колонки заполнены");
                    Environment.Exit(1);
                }
                string article = xlRange.Cells[i, 1].Value2.ToString();
                string name = xlRange.Cells[i, 2].Value2.ToString();
                string price = xlRange.Cells[i, 3].Value2.ToString().Split(',')[0].ToString();
                string picture = xlRange.Cells[i, 6].Value2.ToString();
                string subCategory = xlRange.Cells[i, 7].Value2.ToString();
                var ad = new Ad(article, name, price, picture, subCategory);
                ads.Add(ad);
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            xlWorkbook.Close(false);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            Marshal.ReleaseComObject(xlWorkbook);
            Marshal.ReleaseComObject(xlApp);
        }

        public static void GetAdParams()
        {
            category = File.ReadAllText("category.txt", Encoding.Default);
            currentId = int.Parse(File.ReadAllText("currentId.txt"));
            phone = File.ReadAllText("phone.txt");
            description = File.ReadAllText("description.txt");
        }
    }
}
